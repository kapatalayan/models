package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// StockDay model for table
type StockDay struct {
	ID              uint `gorm:"primary_key"`
	CreatedAt       time.Time
	StockID         uint      `gorm:"not null"`
	TransactionDate time.Time `gorm:"type:date;not null"`
	TradeVolume     int       `gorm:"not null"`
	TradeValue      int64     `gorm:"type:bigint;not null"`
	OpeningPrice    float64   `gorm:"type:real;not null"`
	HighestPrice    float64   `gorm:"type:real;not null"`
	LowestPrice     float64   `gorm:"type:real;not null"`
	ClosingPrice    float64   `gorm:"type:real;not null"`
	Change          float64   `gorm:"type:real;not null"`
	Transaction     int       `gorm:"not null"`
}

// CreateOrUpdate a stock day
func (s *StockDay) CreateOrUpdate(db *gorm.DB, stockID uint, txnDate time.Time, tradeVolume, transaction int, tradeValue int64, openingPrice, highestPrice, lowestPrice, closingPrice, change float64) (rStockDay StockDay, err error) {
	if rStockDay, err = s.FindOne(db, stockID, txnDate); err != nil {
		if err.Error() != CRecordNotFound {
			return
		}
	}

	if rStockDay.ID == 0 {
		rStockDay.StockID = stockID
		rStockDay.TransactionDate = txnDate
		rStockDay.TradeVolume = tradeVolume
		rStockDay.TradeValue = tradeValue
		rStockDay.OpeningPrice = openingPrice
		rStockDay.HighestPrice = highestPrice
		rStockDay.LowestPrice = lowestPrice
		rStockDay.ClosingPrice = closingPrice
		rStockDay.Change = change
		rStockDay.Transaction = transaction
	} else {
		if rStockDay.TradeVolume == tradeVolume && rStockDay.TradeValue == tradeValue {
			return
		}

		// fmt.Printf("stock: %d, %d, %d, %d, %d, %f, %f\n", stockID, rStockDay.TradeVolume, tradeVolume, rStockDay.TradeValue, tradeValue, rStockDay.OpeningPrice, openingPrice)

		rStockDay.TradeVolume = tradeVolume
		rStockDay.TradeValue = tradeValue
		rStockDay.OpeningPrice = openingPrice
		rStockDay.HighestPrice = highestPrice
		rStockDay.LowestPrice = lowestPrice
		rStockDay.ClosingPrice = closingPrice
		rStockDay.Change = change
		rStockDay.Transaction = transaction
	}

	err = db.Save(&rStockDay).Error

	return
}

// FindOne of stock day
func (s *StockDay) FindOne(db *gorm.DB, stockID uint, txnDate time.Time) (rStockDay StockDay, err error) {
	err = db.Where("stock_id=? AND transaction_date=?", stockID, txnDate).First(&rStockDay).Error

	return
}

// GetLast stock day trade
func (s *StockDay) GetLast(db *gorm.DB, stock *Stock) (rStockDay StockDay, err error) {
	err = db.Where("stock_id=?", stock.ID).Order("transaction_date DESC").Limit(1).Find(&rStockDay).Error

	return
}

// GetMovingAverage of stock day
func (s *StockDay) GetMovingAverage(db *gorm.DB, stock *Stock, days float64) (result float64, err error) {
	var stockDays []StockDay
	// SELECT * FROM public.stock_days WHERE stock_id=956 ORDER BY transaction_date DESC LIMIT 20
	if err = db.Where("stock_id=?", stock.ID).Order("transaction_date DESC").Limit(days*2 - 1).Find(&stockDays).Error; err != nil {
		return
	}

	var total float64
	for i := 0; i < len(stockDays); i++ {
		total += stockDays[i].ClosingPrice
	}

	result = total / days

	return
}

// IsExist of stock day
func (s *StockDay) IsExist(db *gorm.DB, stockID uint, txnDate time.Time) (result bool, err error) {
	if _, err = s.FindOne(db, stockID, txnDate); err != nil {
		if err.Error() != CRecordNotFound {
			return
		}
		err = nil
	} else {
		result = true
	}

	return
}
