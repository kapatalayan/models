package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// VersionInfo model for table
type VersionInfo struct {
	ID          uint `gorm:"primary_key"`
	CreatedAt   time.Time
	Version     int    `gorm:"not null"`
	Description string `gorm:"size:1024;not null"`
}

// Create version info
func (v *VersionInfo) Create(db *gorm.DB, ver int, desc string) (rVersionInfo VersionInfo, err error) {
	rVersionInfo = VersionInfo{
		Version:     ver,
		Description: desc}

	err = db.Create(&rVersionInfo).Error

	return
}

// GetVersion of database schema
func (v *VersionInfo) GetVersion(db *gorm.DB) (version int, err error) {
	var vi VersionInfo

	if err = db.Order("id DESC").First(&vi).Error; err == nil {
		version = vi.Version
	} else {
		if err.Error() == CRecordNotFound {
			err = nil
		}
	}

	return
}
