package models

import (
	"fmt"
	"strconv"

	"github.com/jinzhu/gorm"
)

// CRecordNotFound for data not found
const CRecordNotFound string = "record not found"

// ConnectToDatabase operation
func ConnectToDatabase(dbHost, dbUser, dbName, dbPwd string, dbPort int) (db *gorm.DB, err error) {
	db, err = gorm.Open("postgres", "host="+dbHost+" port="+strconv.Itoa(dbPort)+" user="+dbUser+" dbname="+dbName+" sslmode=disable password="+dbPwd)

	return
}

// CREATE DATABASE stock_dev WITH OWNER = nyan_adm;
// CREATE ROLE nyan_adm LOGIN NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION PASSWORD 'nYan#41434281';

// Migration table
func Migration(db *gorm.DB, toVersion int) (err error) {
	var currentVersion, migrationVersion int
	var vi VersionInfo

	if db.HasTable(&VersionInfo{}) == false {
		if err = db.AutoMigrate(&VersionInfo{}).Error; err != nil {
			return
		}
	}

	if currentVersion, err = vi.GetVersion(db); err != nil {
		return
	}

	if toVersion >= 1 && currentVersion < 1 {
		fmt.Println("Migration: 1")

		if db.HasTable(&Stock{}) == false {
			if err = db.AutoMigrate(&Stock{}).Error; err != nil {
				return
			}

			if err = db.Model(&Stock{}).AddUniqueIndex("idx_stocks_code", "code").Error; err != nil {
				return
			}
		}

		migrationVersion = 1
	}

	if toVersion >= 2 && currentVersion < 2 {
		fmt.Println("Migration: 2")

		if db.HasTable(&StockDailyTrade{}) == false {
			if err = db.AutoMigrate(&StockDailyTrade{}).Error; err != nil {
				return
			}

			if err = db.Model(&StockDailyTrade{}).AddIndex("idx_stock_daily_trade_stock_id", "stock_id").Error; err != nil {
				return
			}

			if err = db.Model(&StockDailyTrade{}).AddIndex("idx_stock_daily_trade_transaction_date", "transaction_date").Error; err != nil {
				return
			}
		}

		migrationVersion = 2
	}

	if toVersion >= 3 && currentVersion < 3 {
		fmt.Println("Migration: 3")

		if db.HasTable(&StockDay{}) == false {
			if err = db.AutoMigrate(&StockDay{}).Error; err != nil {
				return
			}

			if err = db.Model(&StockDay{}).AddIndex("idx_stock_day_stock_id", "stock_id").Error; err != nil {
				return
			}

			if err = db.Model(&StockDay{}).AddIndex("idx_stock_day_transaction_date", "transaction_date").Error; err != nil {
				return
			}
		}

		migrationVersion = 3
	}

	if migrationVersion > 0 {
		_, err = vi.Create(db, migrationVersion, "")
	}

	return
}
