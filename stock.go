package models

import "github.com/jinzhu/gorm"

// Stock model for table
type Stock struct {
	gorm.Model
	Code   string `gorm:"size:64;not null"`
	Name   string `gorm:"size:64;not null"`
	TypeID int    `gorm:"not null"`
}

// CreateOrUpdate stock
func (s *Stock) CreateOrUpdate(db *gorm.DB, code, name string, typeID int) (rStock Stock, err error) {
	if rStock, err = s.FindOne(db, code); err != nil {
		if err.Error() != CRecordNotFound {
			return
		}
	}

	if rStock.ID == 0 {
		rStock.Code = code
	} else {
		if rStock.Code == code && rStock.Name == name {
			return
		}
	}

	rStock.Name = name
	rStock.TypeID = typeID

	err = db.Save(&rStock).Error

	return
}

// Find stocks
func (s *Stock) Find(db *gorm.DB, typeID int) (rStocks []Stock, err error) {
	err = db.Where("type_id=?", typeID).Find(&rStocks).Error

	return
}

// FindOne of stock
func (s *Stock) FindOne(db *gorm.DB, code string) (rStock Stock, err error) {
	err = db.Unscoped().Where("code=?", code).First(&rStock).Error

	return
}
