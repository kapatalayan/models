package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// StockDailyTrade model for table
type StockDailyTrade struct {
	ID              uint `gorm:"primary_key"`
	CreatedAt       time.Time
	StockID         uint      `gorm:"not null"`
	TransactionDate time.Time `gorm:"type:date;not null"`
	TotalBuy1       int       `gorm:"not null"`
	TotalSell1      int       `gorm:"not null"`
	Difference1     int       `gorm:"not null"`
	TotalBuy2       int       `gorm:"not null"`
	TotalSell2      int       `gorm:"not null"`
	Difference2     int       `gorm:"not null"`
	TotalBuy3       int       `gorm:"not null"`
	TotalSell3      int       `gorm:"not null"`
	Difference3     int       `gorm:"not null"`
}

// CreateOrUpdate a stock daily trade
func (s *StockDailyTrade) CreateOrUpdate(db *gorm.DB, stockID uint, txnDate time.Time, totalBuy1, totalSell1, difference1, totalBuy2, totalSell2, difference2, totalBuy3, totalSell3, difference3 int) (rStockDailyTrade StockDailyTrade, err error) {
	if rStockDailyTrade, err = s.FindOne(db, stockID, txnDate); err != nil {
		if err.Error() != CRecordNotFound {
			return
		}
	}

	if rStockDailyTrade.ID == 0 {
		rStockDailyTrade.StockID = stockID
		rStockDailyTrade.TransactionDate = txnDate
		rStockDailyTrade.TotalBuy1 = totalBuy1
		rStockDailyTrade.TotalSell1 = totalSell1
		rStockDailyTrade.Difference1 = difference1
		rStockDailyTrade.TotalBuy2 = totalBuy2
		rStockDailyTrade.TotalSell2 = totalSell2
		rStockDailyTrade.Difference2 = difference2
		rStockDailyTrade.TotalBuy3 = totalBuy3
		rStockDailyTrade.TotalSell3 = totalSell3
		rStockDailyTrade.Difference3 = difference3
	} else {
		if rStockDailyTrade.TotalBuy1 == totalBuy1 && rStockDailyTrade.TotalSell1 == totalSell1 && rStockDailyTrade.Difference1 == difference1 {
			return
		}

		rStockDailyTrade.TotalBuy1 = totalBuy1
		rStockDailyTrade.TotalSell1 = totalSell1
		rStockDailyTrade.Difference1 = difference1
		rStockDailyTrade.TotalBuy2 = totalBuy2
		rStockDailyTrade.TotalSell2 = totalSell2
		rStockDailyTrade.Difference2 = difference2
		rStockDailyTrade.TotalBuy3 = totalBuy3
		rStockDailyTrade.TotalSell3 = totalSell3
		rStockDailyTrade.Difference3 = difference3
	}

	err = db.Save(&rStockDailyTrade).Error

	return
}

// FindOne of stock daily trade
func (s *StockDailyTrade) FindOne(db *gorm.DB, stockID uint, txnDate time.Time) (rStockDailyTrade StockDailyTrade, err error) {
	err = db.Where("stock_id=? AND transaction_date=?", stockID, txnDate).First(&rStockDailyTrade).Error

	return
}

// IsExist of stock daily trade
func (s *StockDailyTrade) IsExist(db *gorm.DB, stockID uint, txnDate time.Time) (result bool, err error) {
	if _, err = s.FindOne(db, stockID, txnDate); err != nil {
		if err.Error() != CRecordNotFound {
			return
		}
		err = nil
	} else {
		result = true
	}

	return
}

// GetLastThree stock daily trade
func (s *StockDailyTrade) GetLastThree(db *gorm.DB, stock *Stock) (rStockDailyTrades []StockDailyTrade, err error) {
	err = db.Where("stock_id=?", stock.ID).Order("transaction_date DESC").Limit(3).Find(&rStockDailyTrades).Error

	return
}
